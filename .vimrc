let g:go_version_warning = 0
execute pathogen#infect()
syntax on
filetype plugin indent on

if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif


let _curfile = expand("%:t")
if _curfile =~ "Makefile" || _curfile =~ "makefile" || _curfile =~ ".*\.mk"
  set noexpandtab
else
  set expandtab
  set tabstop=4
  set shiftwidth=4
endif
:set hlsearch
:set incsearch

if &diff
    colorscheme evening
    syn off
endif
